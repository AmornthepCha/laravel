@extends('layouts.master')

@section('content')
    <h1>Add New Task</h1>
    <hr>
    <form action="/tasks" method="post">
        {{ csrf_field()}}
        <div class="form-group">
            <label for="title">Task Title</label>
            <input type="text" class="form-control" id="task-title" name="title">
        </div>
        <div class="form-group">
            <label for="description">Task Description</label>
            <input type="text" class="form-control" id="taskDescription" name="description">
        </div>
        @if($errors->any())
        <div class="alert alert-danger">
            @foreach($error->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </div>
        @endif
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
