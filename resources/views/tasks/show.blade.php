@extends('layouts.master')

@section('content')
    <div class="container">
        <h1>Showing Task {{$task->title}}</h1>

        <div class="jumbotron text-center">
            <p>
                <strong>Task Title: </strong>{{$task->title}}<br>
                <strong>Task Description: </strong>{{$task->description}}
            </p>
        </div>


    </div>
@endsection
