@extends('layouts.master')

@section('content')

<div class="container">
    <h1>Edit Task ID{{$task->id}}</h1>
    <hr>
<form action="{{ url('tasks',[$task->id])}}" method="post">
    <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Task Title</label>
            <input type="text" value="{{$task->title}}" class="form-control" name="title" id="taskTitle">
        </div>
        <div class="form-group">
            <label for="description">Task Description</label>
            <input type="text" value="{{$task->description}}" class="form-control" id="taskDescription" name="description">
        </div>


        {{-- @if($error->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($error->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif --}}
        <button type="submit" class="btn brn-primary">Submit</button>
</form>
</div>

@endsection
